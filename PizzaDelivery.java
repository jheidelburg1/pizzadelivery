import java.util.Scanner;

public class PizzaDelivery {
    public static void main(String[] args) {
        System.out.println("How many pizza topping do you want?");
        Scanner scan = new Scanner(System.in);
        int numOfToppings = scan.nextInt();

        String[] toppings = new String[numOfToppings];
        System.out.println("Great, Enter each topping!");
        scan.nextLine();

        for (int i = 0; i < toppings.length; i++) {
            System.out.print(i + ". "); toppings[i] = scan.nextLine();
        }

        System.out.println("Thank you! Here are the toppings you ordered");

        for (int i = 0; i < toppings.length; i++) {
            System.out.println(i + ". " + toppings[i]);
        }

        System.out.println("Press enter to confirm your order.");
        scan.nextLine();
        System.out.println("Great, a driver is on the way!");
        scan.close();
    }
}
